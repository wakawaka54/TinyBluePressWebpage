
window.onload = repositionImage;

// Crop left once we go below 1500 px
window.onresize = repositionImage;

function repositionImage() {
    var backgroundImgEl = document.querySelectorAll('.background-image');

    if (window.innerWidth < 1500) {
        backgroundImgEl[0].style.marginLeft = '-' + (1500 - window.innerWidth) + 'px';
    }

    repositionFooter(backgroundImgEl[0].clientHeight);
}

function repositionFooter(imageHeight) {
    var footerEl = document.querySelectorAll('.footer');
    var almostFooterEl = document.querySelectorAll('.almost-footer');
    var alsoAlmostFooterEl = document.querySelectorAll('.also-almost-footer');


    footerEl[0].style.top = imageHeight + 'px';
    almostFooterEl[0].style.top = (imageHeight - 75) + 'px';
    alsoAlmostFooterEl[0].style.top = (imageHeight - 30) + 'px';
}